import React, {useState} from 'react'

const TodoForm = (props) => {
    const [task, setTask] = useState('');
    const submitHandler = (event) => {
        event.preventDefault();
        props.addNewTask(task);
        setTask("");
    }
    return (
        <form onSubmit={submitHandler}>
                    <div className="form-row">
                        <div className="col-sm-8">
                        <input 
                        type="text" 
                        className="form-control" 
                        placeholder="task"
                        value = {task}
                        onChange = {(event) => {setTask(event.target.value)}}
                        />
                        </div>
                        <div className="col-sm-4">
                        <button type="submit" className="btn btn-primary btn-block">Add</button>
                        </div>
                    </div>
                </form>
    )
}

export default TodoForm
