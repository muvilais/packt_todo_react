import React from 'react';
import {  
    Link
} from 'react-router-dom'

const TodoItem = (props) => {
    return (
        <li key={props.taskItem.id} className="list-group-item d-flex justify-content-between">
            <span className={props.taskItem.done?"done-text":""} onClick={() => props.statusChangehandler(props.taskItem.id)}>{props.taskItem.title}</span>
            <span>
                <Link to={`/details/${props.taskItem.id}`} ><i className="fa fa-eye" aria-hidden="true"></i></Link> &nbsp;
                <i onClick={() => props.deleteHandler(props.taskItem.id)} className="fa fa-trash-o" aria-hidden="true"></i> 
            </span>
            
            </li>
    )
}

export default TodoItem
