import React, {useState, useEffect} from 'react'
import axios from 'axios';
const TodoDetails = (props) => {
    const [state, setstate] = useState({})
    const [title, settitle] = useState("")
    const [description, setdescription] = useState("")
    useEffect(() => {
        axios.get(`http://127.0.0.1:8000/api/todo/${props.match.params.id}`)
        .then( response => {  
            setstate(response.data.data);
            settitle(response.data.data.title);
            setdescription(response.data.data.description);

        })
        .catch( error => console.log(error))
    }, [props.match.params.id])
   
    const submitHandler = (event) => {
        event.preventDefault();
      
        props.updateTask(title,description,props.match.params.id);
    }
    return (
        <div>
            <form onSubmit={submitHandler}>
            <div className="form-group">
                
                <input 
                type="text" 
                className="form-control"
                onChange={(event) => {settitle(event.target.value)}} 
                value={title} 
                />
            </div>
            <div className="form-group">
                <textarea 
                className="form-control" 
                placeholder="Description" 
                onChange={(event) => {setdescription(event.target.value)}} 
                value={description}
                rows="3"></textarea>
            </div>
            <button type="submit" className="btn btn-primary btn-block">Update</button>
            </form>
        </div>
    )
}

export default TodoDetails
