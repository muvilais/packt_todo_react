import axios from 'axios';
import React, {useState, useEffect} from 'react'
import TodoItem from './TodoItem';
import loader from '../loader.gif';
import TodoForm from './TodoForm';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom'
import TodoDetails from './TodoDetails';


const Home = () => {
    const [tasks, settasks] = useState([]);

    useEffect(() => {
        fetchData();
    }, [])

    const fetchData = () => {
       
        axios.get('http://127.0.0.1:8000/api/home')
        .then( response => {  
            settasks(response.data.data);
        })
        .catch( error => console.log(error))
    }
    const addNewTask = task => {
        axios.post('http://127.0.0.1:8000/api/add-todo', {
           title : task 
        })
        .then( response => {
            fetchData();
        })
        .catch( error => console.log(error))      
    }
    const updateTask = (title,description,id) => {
        axios.post(`http://127.0.0.1:8000/api/update/${id}`, {
           title : title,
           description : description
        })
        .then( response => {
            fetchData();
        })
        .catch( error => console.log(error))    
    }
    const deleteHandler = (id) => {
        axios.delete(`http://127.0.0.1:8000/api/delete/${id}`)
        .then( response => {
            fetchData();
        })
        .catch( error => console.log(error))
    }
    const statusChangehandler = (id) => {
        axios.get(`http://127.0.0.1:8000/api/status-change/${id}`)
        .then( response => {
            fetchData();
        })
        .catch( error => console.log(error))
    }
    return (
        <div className="container">
            <div className="row">
            <Router>
            <div className="col-md-6 offset-md-2 todo-container p-4 mt-4">
            <Link to="/" className="btn btn-primary my-3"><i className="fa fa-home" aria-hidden="true"></i></Link>
                <TodoForm  addNewTask={addNewTask}/>
                <div className="col-sm-12 p-0 mt-4">     
                    <ul className="list-group">
                        { tasks.map(taskItem => <TodoItem key={taskItem.id} taskItem={taskItem} statusChangehandler={statusChangehandler}  deleteHandler={deleteHandler}/>)}
                    </ul> 
                </div>
            </div>
            <div className="col-md-4 todo-container p-4 mt-4">
            <Switch>    
                
                <Route exact path="/" render={()=>(<div></div>)} />
                <Route path="/details/:id" render={(props) => <TodoDetails updateTask={updateTask} {...props} />} />
            </Switch>
                
            </div>
        </Router>
            
            
            </div>
            
        </div>
    )
}

export default Home
